**Git Rep**: https://bitbucket.org/Intall/casexe_test  
**Damp DB**:  Во вложении и в корневой папке проекта в репозитории /casexe_test.sql  
**Diagramm**: Во вложении и в корневой папке проекта в репозитории /Casexe app.pdf  
**Website**: http://casexe.renderlife.ru  
**Login site**: user_test  
**Pass site:** user_test  


## Примечание
1. Дамп базы данных приложил так как миграции не делал.
2. Параметры диапазонов, коэффициентов, текстовых сообщений задаются в /config/params.php по хорошему надо сделать отдельную страницу для админа для настроек
3. Количество денег (таблица: money, пользователь: admin, т.е. админ как бы является держателем банка средств для розыгрыша) и виды призов (таблица: things_types) и их количество, задается в базе данных.
4. Скрипт для перевода средств на реальные счета пользователей находится /commands/SendMoneyController.php и запускается командой, с указанием количество переводов за раз ``php yii send-money/index --sendPart=3``
5. Файл для тестирования Тестирование метода (Gift::convertMoneyToBonus) перевода денег в бонусы по коэффициенту находится /tests/unit/models/GiftTest.php и запускается командой Screen: [https://s.mail.ru/KsaK/ao4mZaysf](https://s.mail.ru/KsaK/ao4mZaysf) ```vendor/bin/codecept run tests/unit/models/GiftTest.php```
6. Нужно дорабатывать моменты связанные с безопасностью ajax, сделал минимально так как в задаче этого не требовалось.
7. Если в ходе случайного выбора оказалось что нет возможности выдать приз (закончились деньги или кончились предметы) то генерация запускается снова предварительно деактивируя недоступный приз. Генерация длится до тех пор пока доступный приз не будет найден. Если все закончилось то бонусы есть всегда, они и выдаются.
8. Если кол-во денег выбранное случайно попадает в диапазон но оно больше чем доступна на счету компании (админа), то выдается оставшаяся сумма.
9. При переводе денег в бонусы, значение округляется в большую сторону. Например если денег к выдаче положено 1$ то бонус он может получить также 1.
10. В базе данных есть таблицы в которых блокируются деньги или предметы на момент когда пользователь принимает решение о конвертации денег в бонусы или отказе от приза. Это подробно изображено на схеме.


