<?php

return [
    'adminEmail' => 'admin@example.com',
    'adminId' => 1,
    'intervalGiftMoney' => [1, 20],
    'intervalGiftBonus' => [5, 20],
    'coefficientToBonus' => 0.5,
    // TODO: Can be stored in a database
    'giftTypes' => [
        'money' => [
            'name' => 'Money gift',
            'available' => true,
            'title' => 'You won a monetary prize in the amount of',
            'message' => 'You can convert them into loyalty points taking into account coefficient',
            'exchangeBonus' => 'To convert into bonuses',
            'cancel' => 'To leave money',
        ],
        'things' => [
            'name' => 'Things gift',
            'available' => true,
            'title' => 'You won a prize',
            'message' => 'You can refuse or accept the gift it having pressed the buttons below',
            'take' => 'Take the gift',
            'cancel' => 'To refuse a gift',
        ],
        'bonus' => [
            'name' => 'Bonus gift',
            'available' => true,
            'title' => 'You received as a gift bonuses in the amount of',
        ],
    ],
];
