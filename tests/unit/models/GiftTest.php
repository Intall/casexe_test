<?php

namespace tests\models;

use app\models\Gift;

class GiftTest extends \Codeception\Test\Unit
{
    public function testConvertMoneyToBonus()
    {
        expect_that($bonus = Gift::convertMoneyToBonus(100));
        expect($bonus)->equals(50);

        expect_not(Gift::convertMoneyToBonus(0));
    }
}
