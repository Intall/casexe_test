<?php
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Money;
use app\models\TransferMoney;

class SendMoneyController extends Controller
{
    public $obMoney;
    public $obTransferMoney;
    public $sendPart;

    /**
     * {@inheritdoc}
     */
    public function options($actionId)
    {
        return ['sendPart'];
    }

    /**
     * {@inheritdoc}
     */
    public function actionIndex()
    {
        if (empty($this->sendPart)) {
            $this->sendPart = 2;
        }

        $idsTransferDone = [];
        // Receive N records from the table of transactions
        $this->obMoney = new Money;
        $this->obTransferMoney = new TransferMoney;
        $arTransfers = $this->obTransferMoney->getTransferMoneyNotSend($this->sendPart);
        if (!empty($arTransfers)) {
            // Foreach transfers and we send to accounts of users
            foreach ($arTransfers as $key => $arTransfer) {
                if (self::requestAPIBank($arTransfer['user_id'], $arTransfer['amount'])) {
                    // Add money for the score of the user in a private office
                    $arMoneyUser = $this->obMoney->getMoney($arTransfer['user_id']);
                    $this->obMoney->updateMoney($arTransfer['user_id'], $arMoneyUser['amount'] + $arTransfer['amount']);
                    // Collect all ids of successfully sent transactions
                    $idsTransferDone[] = $arTransfer['id'];
                }
            }
            // Change the status on it is sent at successful transactions
            $this->obTransferMoney->updateStatusTransferMoney($idsTransferDone, 'send');
            echo  'Money sended ' . count($idsTransferDone) . "\n";
        } else {
            echo  'Empty transfer' . "\n";
        }

        return ExitCode::OK;
    }

    /**
     * {@inheritdoc}
     */
    public function requestAPIBank($userId = 0, $amount = 0)
    {
        // To transfer money to the account of the user
        // HTTP request to API bank
        if (!empty($userId)) {
            // Get to the user account number in a Bank
            $accountNumberBank = '9999-9999-9999-9999';
            // Send $amount money through API
            if ($sendOk = 1) {
                return true;
            }
        }

        return false;
    }
}
