-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: 185.143.173.56
-- Время создания: Ноя 13 2018 г., 00:42
-- Версия сервера: 5.7.13
-- Версия PHP: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `casexe_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `bonus`
--

CREATE TABLE `bonus` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `bonus`
--

INSERT INTO `bonus` (`id`, `user_id`, `amount`) VALUES
(1, 2, 828),
(2, 1, 13);

-- --------------------------------------------------------

--
-- Структура таблицы `exchange_money_to_bonus`
--

CREATE TABLE `exchange_money_to_bonus` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key_exchange` varchar(70) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `exchange_money_to_bonus`
--

INSERT INTO `exchange_money_to_bonus` (`id`, `user_id`, `key_exchange`, `amount`) VALUES
(1, 2, '6df839fbc431c413f4fc713301a755f5f3f911f88448bdb4d001becf2104004c', 8),
(2, 2, '222c33dcd593d7c5b1435233afdcd2c41ea90b691db9beca855eaa337369464d', 1),
(3, 2, 'c5a25fb01364b35369c6d63f13a03350a5b3a45298c662fa67ae26a80cd0be10', 8),
(7, 2, 'ad2d98700fb20a8da66c948c4ecb7ecf365c7a08eb3fc14509850cc394a1e6ba', 5),
(8, 2, '0a251ebf0194c0d2c85cd33913baad3db9624fbceab16a21f6146a6edf9687a6', 6),
(9, 2, '80e4e8ec81415f3745a917de4769a40a4f670e072db61a0098eecd700c5156b6', 4),
(18, 2, '294d6bd9815d526f023f279b2df9ff3d9cd1527cc1d98f6c8fb0cc00f1a3e7b0', 2),
(19, 2, 'df0fc371325d736073f6bacfff15b1d23941b0bbe154ce8979d2fed4047ca883', 1),
(20, 2, '14fe840c7761b0f5b74bf9f79015c384462854dc0483719ef8a6a4c2afa63925', 5),
(21, 2, '763124b6be4b5d31b11bed20eddd7f342d59e1fa0f531c3094f26b8b21934a67', 4),
(22, 2, 'e9143fe4eb501f2e747546228dfbccf5177e7cf12a4b700769a31f6456f6b369', 10),
(23, 2, '86c47f91c41e852cac9fcd2a32c431f0c9188d71486f007c97cb812c9d4fb2a3', 6),
(24, 2, 'fd198cc938872d35d28443c20eedc03f65904acd647fbb7fc4cc37d88bb22b62', 7),
(25, 2, '2f554359d33e127e1162dd3fd52f8ef43b6ea0eb43bcbeb5f0d7ac4de8539017', 8),
(27, 2, 'dbaeefdd0520405cc477ed2af69eee9b2f7072ae558b32b3d7dcf2f86a3fca6b', 10),
(28, 2, 'df4f4ad9856699e0c265335df8072871fd7b7c2d5391b8a31096012fc765e168', 9),
(46, 2, '5b763a038a8c5eccebfbe300be54bc310ab428d09dfe15231bf6f31472d415d1', 6),
(47, 2, '46759836902be5bf1be3db94692711cd4578494eaa1b569a346e0b46a56c7164', 9),
(48, 2, '16fcdb3ed22c1896f0c466c0dc4c07bd465c361be8021b0dd8fd3a30ffbf7f6f', 11),
(49, 2, '5b1b2691da5322eca59a291b61690afb7ba59d5b875351e8479e56dd28f4c1b8', 1),
(50, 2, '4953694145ef25fd72348ee719637546e3952c108a6002c03c2d412c46d33a80', 15),
(51, 2, 'fdeecaeeed311d1aa5daee122cad6283d8d10d36366f08e03dfbf489f32b6e32', 10),
(52, 2, 'c0b5857cfcdb92bc7b4496da2699db5c2342f3ddc415edff2b113bbb206fceab', 4),
(53, 2, '7f0c914721a1e32b39fa684bb8ffe5daa87f0bb1469deec4093d924eb9f1447e', 6),
(54, 2, 'e57ff97077e13222b1c5d3e40842486ea59f2a71baa84332bfa82453b19f6d24', 3),
(55, 2, '89be3c644bb81ce883fdbc1e930ca453671efe790795878a801ab3c4d440a8e4', 20),
(56, 2, 'b5f254f637a5562176c3c72563cb2d9f0e9b854c9261444ce777ff670bbacd28', 18),
(57, 2, 'd5e726f511bc0df8e3e7c0260d63c1952fc64b1cf4397478d5865bf954a8d080', 16),
(65, 2, '8bf885511b930babc421d23b55c3daef37ecb5bf21c71673c38d84796c54df55', 2),
(66, 2, '3b6e400c107a684902e11079a30f95e1768f72a672655d3befd0a6548734e837', 17),
(67, 2, 'dd5d887e7744517d1413e4dcc4b983d7a4d4872299b0e85dfa4bd5a08ee00490', 1),
(69, 2, 'd575e6b98418b5602987b72b899264af4ba4d3d8cab596b0c5efced9f9cb6047', 19),
(107, 1, '889ace5cdfa6b1416e02a1fd5bbe49c749fe0d6885699c91bcfa331fc7585501', 18),
(110, 2, '52b9388d4cecdbf2d723fc5da1a20b7d90ff9c198f3803b77256b74cb67ae52e', 8);

-- --------------------------------------------------------

--
-- Структура таблицы `money`
--

CREATE TABLE `money` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `money`
--

INSERT INTO `money` (`id`, `user_id`, `amount`) VALUES
(1, 1, 942),
(2, 2, 95);

-- --------------------------------------------------------

--
-- Структура таблицы `things`
--

CREATE TABLE `things` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `things`
--

INSERT INTO `things` (`id`, `user_id`, `data`) VALUES
(2, 2, 'a:2:{i:2;a:2:{s:4:"name";s:6:"iPhone";s:6:"amount";i:4;}i:10;a:2:{s:4:"name";s:12:"Movie ticket";s:6:"amount";i:4;}}'),
(3, 1, 'a:1:{i:10;a:2:{s:4:"name";s:12:"Movie ticket";s:6:"amount";i:1;}}');

-- --------------------------------------------------------

--
-- Структура таблицы `things_types`
--

CREATE TABLE `things_types` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `things_types`
--

INSERT INTO `things_types` (`id`, `name`, `amount`) VALUES
(1, 'MacBook', 5),
(2, 'iPhone', 6),
(10, 'Movie ticket', 44);

-- --------------------------------------------------------

--
-- Структура таблицы `transfer_money`
--

CREATE TABLE `transfer_money` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transfer_money`
--

INSERT INTO `transfer_money` (`id`, `user_id`, `amount`, `status`) VALUES
(1, 2, 5, 'send'),
(3, 2, 16, 'send'),
(4, 2, 14, 'send'),
(5, 1, 13, 'send'),
(6, 1, 18, 'send'),
(7, 2, 20, 'send'),
(8, 2, 8, 'send'),
(9, 2, 11, 'send'),
(10, 2, 2, 'send'),
(11, 2, 13, 'send'),
(12, 2, 18, 'send'),
(13, 2, 6, 'send'),
(14, 2, 2, 'send'),
(15, 2, 13, 'send'),
(16, 2, 2, 'send'),
(17, 2, 13, 'send'),
(18, 2, 1, 'send'),
(19, 2, 6, 'send'),
(20, 2, 20, 'turn'),
(21, 2, 19, 'turn'),
(22, 2, 19, 'turn'),
(23, 2, 20, 'turn'),
(24, 1, 20, 'turn'),
(25, 2, 15, 'turn'),
(26, 1, 18, 'turn'),
(27, 2, 5, 'turn');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `access_token` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `auth_key`, `access_token`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL),
(2, 'user_test', '4c73ec194152cad686f51543cfdfaa35', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `warehouse_things`
--

CREATE TABLE `warehouse_things` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `id_things` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `key_action` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `warehouse_things`
--

INSERT INTO `warehouse_things` (`id`, `user_id`, `name`, `id_things`, `amount`, `status`, `key_action`) VALUES
(11, 2, 'iPhone', 2, 1, 'to_send', '01125a7a3bd18e20ad6b1b8989e2acb1b083cb364d24aa2e10ecd2f81fdc0a68'),
(12, 2, 'Movie ticket', 10, 1, 'to_send', '18d3e984a20b1d0e93158374e3cb8219ecfc39bc79540365815bd9d821706ad4'),
(13, 2, 'Movie ticket', 10, 1, 'to_send', '46bcd3c5a40ab66b4e56948bb31f0cd153e9b3962e0a56112e0b7d8524dfa87e'),
(14, 2, 'Movie ticket', 10, 1, 'to_send', '137e95a7ac34efb725c93deac3c76b89937da74baac4ac94b095951743ddc614'),
(15, 2, 'iPhone', 2, 1, 'to_send', '85b4d271baecdbf62e65bbc33e263255b445818464f6a3bece5b0979919b2d94'),
(16, 2, 'Movie ticket', 10, 1, 'to_send', '83725f8f4cbe9abe676642a4eaf5640ec33af90c786cd309da4c975d09a2f40b'),
(17, 2, 'MacBook', 1, 1, 'to_send', '50d050320d427cafe1ef00562f36e190cab61ddaadd7dab7513bfde671908ef3'),
(18, 2, 'MacBook', 1, 1, 'to_send', 'bbe54860a52370b4e161a39aabd2c470d0a9b336d36518397e418820a9a761f2'),
(19, 2, 'Movie ticket', 10, 1, 'to_send', 'af2c5f498f386a8019a7672a44b8f5f8b3bf60288c65d4c6c6d7b0495d830133'),
(20, 2, 'Movie ticket', 10, 1, 'to_send', '377a97f45d87d777ed641010c9eb1a928c0e0d64324abc016f088e579239f0b3'),
(21, 2, 'iPhone', 2, 1, 'reserve', 'b1209bce3e0def8bf9c55e69fd1b9051245af8126a5cce9cf7b38bc869983883'),
(22, 2, 'iPhone', 2, 1, 'reserve', 'f4ff07c1e8097f3125e4efd88858286b95c5751b02952e14a7fcb3823cc5455b'),
(23, 2, 'Movie ticket', 10, 1, 'reserve', 'e6b7f32bf53f8dd275241c95401f64c36af1a41286834741176ce8a9d33cc6e7'),
(24, 2, 'Movie ticket', 10, 1, 'reserve', '748e29bf94e1d92b72adb7e1350aa4d9e1a05a25a61bbc9cd1efbd2250065ce7'),
(25, 2, 'Movie ticket', 10, 1, 'to_send', '344e942b6f0a763888bb736dc21e19a702a63c0dcbcb47bbc772fd31d7d60264'),
(26, 2, 'Movie ticket', 10, 1, 'to_send', '0241eb6951f8168071563d819e4f44a27d74c0f79883f1d0153fccea0ffa5461'),
(27, 2, 'Movie ticket', 10, 1, 'to_send', 'e5895c95d1d6c71597560dbb9322c6a1472bc1d428660ca720895980257644d7'),
(28, 2, 'Movie ticket', 10, 1, 'to_send', '1e2f9b36219e378331bb3f3202bad70af16b722e7d683892df0b8284c5843a8b'),
(29, 2, 'Movie ticket', 10, 1, 'to_send', 'ccf8e73c4c4c0bbe50535926e1fa5287f1d64ebf36f068a6ce13223bcc4df138'),
(30, 2, 'Movie ticket', 10, 1, 'to_send', 'fcce30a7d62e0b57173740210775411c303c4a3cfc5c14b6152d6a50242b8687'),
(31, 2, 'Movie ticket', 10, 1, 'to_send', 'e0abfe101475b0bf5cd0c614609abc220829988d4c7ab6b7f0bf9a7739c97295'),
(35, 2, 'Movie ticket', 10, 1, 'to_send', 'ad721e5b4cfe991e167650b9ea0a990a9e868e4b37a67af5ceed0ce39d044af1'),
(37, 2, 'Movie ticket', 10, 1, 'to_send', 'f2f8aece764974b7a663167ca8ca3f1c83c8d56207d5179f05fa7108fab1eaa8'),
(38, 2, 'Movie ticket', 10, 1, 'to_send', 'cce5b67d4d7b5e36399faaabdd327d2047fbfb17ea3c6f4e9d0aae17d5a74014'),
(39, 2, 'Movie ticket', 10, 1, 'to_send', '3f5daaca2775f9f99260336e6276f2030fbdace40915cbf846b7a140d4cc647a'),
(41, 2, 'iPhone', 2, 1, 'reserve', '682691d73ce242bdef10f7b49f05db0731d80f418b8d5c34b8326d652e8c7ffc'),
(42, 2, 'iPhone', 2, 1, 'to_send', '6d9895a8ed6173c59283e4cfd8e078851f06728b55215b2da47fa0527cb4dcdb'),
(43, 2, 'iPhone', 2, 1, 'reserve', '83c98afc7ac4e252f606fa71127a64bd503c6a02b82ddce01c0bb6c661c2d855'),
(44, 2, 'iPhone', 2, 1, 'reserve', '7b665cc7e097426a03ee746e88b20c94772ecc353370db4cd147d08ca6d59957'),
(45, 2, 'iPhone', 2, 1, 'reserve', '3269df47ce77d9e4bfebbf1375434261ce4914319e7fb7805d8076af86e610fd'),
(46, 2, 'Movie ticket', 10, 1, 'reserve', 'e8cc403d636e599da2f86de68e3e87c5483c8a03e1d80d9e6ed1ffd9b1e38c51'),
(47, 2, 'Movie ticket', 10, 1, 'reserve', 'fb1e2227b82bed0c1897dc2b76314bca6ec079d13eb25722c06e39c63cd099d4'),
(48, 2, 'iPhone', 2, 1, 'reserve', '37a8d7d1691aba88adb6880dc4351880bd7585f36424d205406439639713dcbc'),
(49, 2, 'Movie ticket', 10, 1, 'reserve', '7c1d37589fe460a60d8647b6579d5e31f8a1ed37b6d23e75be8ef524557c3c22'),
(50, 2, 'iPhone', 2, 1, 'reserve', '40685275513e2c80b6e15768a1f7eefa8179e543291e9ada495e4cda2f0f96ba'),
(51, 2, 'Movie ticket', 10, 1, 'reserve', '9f09b4b4b7aa29ad2b2db28b4d5cbc5b7c80d0a73753675093f29cdfe49fb77b'),
(52, 2, 'Movie ticket', 10, 1, 'reserve', '14d4c19873095fa9d8f64cff1b07a63e4003ab3cefbef9e0da04b7799ec52730'),
(53, 2, 'Movie ticket', 10, 1, 'reserve', '231c331b46ef2e0e60d708b442083504b65d102da1f309c4923498831c4da896'),
(54, 2, 'iPhone', 2, 1, 'reserve', '68a11aaf4c728b4958d594e09029bb1c1b0e7c538d59dd848cad2b9ba70d6c3f'),
(55, 2, 'iPhone', 2, 1, 'to_send', '18aa8999c27ee6e2f9f68e7497c95d3ec07495a3f2cb6cb940662deb75042ee5'),
(56, 2, 'Movie ticket', 10, 1, 'to_send', '72ef69de27584149410ef1270e5204ba57495e6826fbc03522f41b00bc8e4cec'),
(57, 2, 'Movie ticket', 10, 1, 'reserve', 'be2b7dfea7c56c6b38c2ec9898d630d49383da4f9de10ebe30c7f26d19719e09'),
(58, 2, 'iPhone', 2, 1, 'reserve', '946ed85180de1fe9e0b10f9a3c4218e830c61bae606eff2908accd61e9de4baf'),
(59, 2, 'Movie ticket', 10, 1, 'reserve', 'b120dac75cd4190b935eed0d45a9cfc973f0d149f904a9148c6370523a8fe1e2'),
(60, 2, 'iPhone', 2, 1, 'reserve', 'aa3bc40e5d9b096be2b90139bdd3a1dff22902667ed38ac70a0e931b1d91602e'),
(61, 2, 'Movie ticket', 10, 1, 'reserve', 'c09023a3c4899b2f4a7592133fe7ad159f73e6620bab250641d3d07de633f5f5'),
(62, 2, 'iPhone', 2, 1, 'to_send', '6d33bd3d26f0241ab0ff6c2f9a2ca92b3e407cb9b029a220c0b513d778669d25'),
(63, 2, 'iPhone', 2, 1, 'to_send', '8d3e18941de5dde7b60465a8d8bb8f512c9202aca88c35d4d5c2bf5d24a641de'),
(64, 2, 'iPhone', 2, 1, 'to_send', 'c759091cb78d26f5e5dbbb9368f744f1ea6808e564b5b4317c87f007146487ab'),
(65, 2, 'iPhone', 2, 1, 'reserve', '4c397a1bdb902a3034185bf9622d884ec19e39b6481ab35b859119c91a12a1d0'),
(66, 2, 'iPhone', 2, 1, 'to_send', '5e10692ebf217ed4abeba01ac91bf2073c1a2143fc50e8f84f2bb0b255ede563'),
(68, 2, 'iPhone', 2, 1, 'reserve', 'a8fd4b5b406898956691c76e077016a46d6644a124cba0ec5e1c08017bb473e3'),
(73, 2, 'iPhone', 2, 1, 'to_send', 'baabe22117645bf6b80828743ad92aad52b6adcfb586464ba827131585fddf95'),
(76, 2, 'iPhone', 2, 1, 'to_send', 'a94537a52351e6a36e24a5f9cf8dc0823410ccc72877d9d21c902c0bcfe1e516'),
(77, 2, 'iPhone', 2, 1, 'to_send', 'e1fdad530351ffa7b4eb12ef5aa6f4b96159abcc8853c8a67d74880e0aa287b3'),
(79, 1, 'Movie ticket', 10, 1, 'to_send', 'aaf110c9e645c626f243b74fe1b1d634c09838ee2c6d28d15a766b57de999320'),
(82, 2, 'Movie ticket', 10, 1, 'to_send', '0bc4103dc398dfcc3e49938bc5b594ad541484c68ae92e0dee52d7c6995d397c'),
(83, 2, 'Movie ticket', 10, 1, 'to_send', '746d426ec96bb43221c69511a7344fedb75624ce1edbbb6dac7b37516cefcf4c'),
(84, 2, 'Movie ticket', 10, 1, 'to_send', '2e5084b4713355e5e136c5471bd87a400e35ea1f7a6bc05dddfa9930fc3d0fb5'),
(85, 2, 'iPhone', 2, 1, 'to_send', 'ec8d9d622b4f90b8801e41d4f2b2425c7b16e37814d2a0e10aaa04ab87adbd1f'),
(86, 2, 'iPhone', 2, 1, 'to_send', 'b79e57963269c57538c5097e331e5daa856b387b3399da60926eb0b715c68eff'),
(87, 2, 'Movie ticket', 10, 1, 'to_send', 'f084334f0c6bbd63b4bf26c4dadf71d67b6ec4a7ad4b6faadfba948a7e1a0955'),
(88, 2, 'Movie ticket', 10, 1, 'reserve', '1d45304eb501a9cb25b04a5ff915f37885752367144b90b25b66dd650ceffaf9'),
(90, 2, 'iPhone', 2, 1, 'to_send', '5b609527645b01e8c8be8880f8c76ddc976db7e4c3e7b3b6b1887b0c6d5520b6');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `exchange_money_to_bonus`
--
ALTER TABLE `exchange_money_to_bonus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `money`
--
ALTER TABLE `money`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `things`
--
ALTER TABLE `things`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `things_types`
--
ALTER TABLE `things_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transfer_money`
--
ALTER TABLE `transfer_money`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `warehouse_things`
--
ALTER TABLE `warehouse_things`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `exchange_money_to_bonus`
--
ALTER TABLE `exchange_money_to_bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT для таблицы `money`
--
ALTER TABLE `money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `things`
--
ALTER TABLE `things`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `things_types`
--
ALTER TABLE `things_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `transfer_money`
--
ALTER TABLE `transfer_money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `warehouse_things`
--
ALTER TABLE `warehouse_things`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
