<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\Gift;

class GiftController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'only' => ['logout'],
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['get-random-gift', 'exchange-money-to-bonus', 'take-money', 'take-things', 'cancel-things'],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actionGetRandomGift()
    {
        $model = new Gift();
        $message = [];
        $message = $model->getRandomGift();

        $type = !empty($message['name']) ? $message['name'] : null;

        $gifts = $model->getAllGift();
        $this->layout = false;
        $html = $this->render('table', ['gifts' => $gifts, 'message' => $message]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return compact('html', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function actionExchangeMoneyToBonus()
    {
        $model = new Gift();
        $model->exchangeMoneyToBonus($_POST['key']);

        $gifts = $model->getAllGift();
        $this->layout = false;
        $html = $this->render('table', ['gifts' => $gifts]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return compact('html');
    }

    /**
     * {@inheritdoc}
     */
    public function actionTakeMoney()
    {
        $model = new Gift();
        $model->takeMoney($_POST['key']);

        $gifts = $model->getAllGift();
        $this->layout = false;
        $html = $this->render('table', ['gifts' => $gifts]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return compact('html');
    }

    /**
     * {@inheritdoc}
     */
    public function actionTakeThings()
    {
        $model = new Gift();
        $model->takeThings($_POST['key']);

        $gifts = $model->getAllGift();
        $this->layout = false;
        $html = $this->render('table', ['gifts' => $gifts]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return compact('html');
    }

    /**
     * {@inheritdoc}
     */
    public function actionCancelThings()
    {
        $model = new Gift();
        $model->cancelThings($_POST['key']);

        $gifts = $model->getAllGift();
        $this->layout = false;
        $html = $this->render('table', ['gifts' => $gifts]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return compact('html');
    }
}