$(document).ready(function() {
    $(".js-get-gift" ).click(function() {
        if ($(this).attr('disabled') == undefined) {
            $.ajax({
                type: "POST",
                url: 'index.php?r=gift/get-random-gift',
                dataType: 'json',
                success: function (data) {
                    $(".js-body-content").html(data.html);
                    if (data.type !== 'bonus') {
                        $(".js-get-gift").attr('disabled', 'disabled');
                    }
                },
            });
        }

        return false;
    });

    $(".site-index").on('click', '.js-exchange-money-to-bonus', function() {
        $.ajax({
            type: "POST",
            url: 'index.php?r=gift/exchange-money-to-bonus',
            data: 'key=' + $(this).data('key'),
            dataType: 'json',
            success: function (data) {
                $(".js-body-content").html(data.html);
                $(".js-get-gift").removeAttr('disabled');
            },
        });

        return false;
    });

    $(".site-index").on('click', '.js-take-money', function() {
        $.ajax({
            type: "POST",
            url: 'index.php?r=gift/take-money',
            data: 'key=' + $(this).data('key'),
            dataType: 'json',
            success: function (data) {
                $(".js-body-content").html(data.html);
                $(".js-get-gift").removeAttr('disabled');
            },
        });

        return false;
    });

    $(".site-index").on('click', '.js-take-things', function() {
        $.ajax({
            type: "POST",
            url: 'index.php?r=gift/take-things',
            data: 'key=' + $(this).data('key'),
            dataType: 'json',
            success: function (data) {
                $(".js-body-content").html(data.html);
                $(".js-get-gift").removeAttr('disabled');
            },
        });

        return false;
    });

    $(".site-index").on('click', '.js-cancel-things', function() {
        $.ajax({
            type: "POST",
            url: 'index.php?r=gift/cancel-things',
            data: 'key=' + $(this).data('key'),
            dataType: 'json',
            success: function (data) {
                $(".js-body-content").html(data.html);
                $(".js-get-gift").removeAttr('disabled');
            },
        });

        return false;
    });
});
