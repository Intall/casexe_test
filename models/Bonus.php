<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bonus".
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required'],
            [['user_id', 'amount'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
        ];
    }

    public static function getBonus($userId)
    {
        return static::findOne(['user_id' => $userId]);
    }

    public static function updateBonus($userId, $value)
    {
        $bonus = static::findOne(['user_id' => $userId]);
        if (empty($bonus)) {
            $bonus = new Bonus();
            $bonus->user_id = $userId;
            $bonus->amount = $value;
            $bonus->save();
        } else {
            $bonus->amount = $value;
            $bonus->save();
        }

        return $bonus;
    }
}
