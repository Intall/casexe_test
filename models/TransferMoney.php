<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transfer_money".
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 */
class TransferMoney extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transfer_money';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'status'], 'required'],
            [['user_id', 'amount'], 'integer'],
            [['status'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'status' => 'Status',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function addTransferMoney($userId, $value)
    {
        $transfer = new TransferMoney();
        $transfer->user_id = $userId;
        $transfer->amount = $value;
        $transfer->status = 'turn';
        $transfer->save();

        return $transfer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTotalTransferMoney($userId)
    {
        $totalTransferMoney = 0;
        $arTransfer = static::findAll(['user_id' => $userId, 'status' => 'turn']);
        if (!empty($arTransfer) && !isset($arTransfer['amount'])) {
            foreach ($arTransfer as $key => $value) {
                $totalTransferMoney += $value['amount'];
            }
        }

        return $totalTransferMoney;
    }

    /**
     * {@inheritdoc}
     */
    public static function getTransferMoneyNotSend($limit)
    {
        $transfers = static::find()->where(['status' => 'turn'])->orderBy('id ASC')->limit($limit)->all();

        return $transfers;
    }

    /**
     * {@inheritdoc}
     */
    public static function updateStatusTransferMoney($ids, $status = 'send')
    {
        $transfers = static::updateAll(['status' => $status], ['in', 'id', $ids]);
        return $transfers;
    }


}
