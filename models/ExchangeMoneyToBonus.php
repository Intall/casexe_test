<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "exchange_money_to_bonus".
 *
 * @property int $id
 * @property int $user_id
 * @property string $key_exchange
 * @property int $amount
 */
class ExchangeMoneyToBonus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'exchange_money_to_bonus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'key_exchange', 'amount'], 'required'],
            [['user_id', 'amount'], 'integer'],
            [['key_exchange'], 'string', 'max' => 70],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'key_exchange' => 'Key Exchange',
            'amount' => 'Amount',
        ];
    }

    public static function addExchangeMoneyToBonus($userId, $key, $value)
    {
        $exchange = new ExchangeMoneyToBonus();
        $exchange->user_id = $userId;
        $exchange->key_exchange = $key;
        $exchange->amount = $value;
        $exchange->save();

        return $exchange;
    }

    public static function removeExchangeMoneyToBonus($key)
    {
        $exchange = static::findOne(['key_exchange' => $key]);
        $exchange->delete();

        return $exchange;
    }

    public static function getExchangeMoneyToBonus($key)
    {
        $arExchange = static::findOne(['key_exchange' => $key]);
        
        return $arExchange;
    }
}
