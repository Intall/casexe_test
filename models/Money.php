<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "money".
 *
 * @property int $id
 * @property int $user_id
 * @property int $amount
 */
class Money extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'money';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required'],
            [['user_id', 'amount'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getMoney($userId)
    {
        $arMoney = static::findOne(['user_id' => $userId]);
        
        return $arMoney;
    }

    /**
     * {@inheritdoc}
     */
    public static function updateMoney($userId, $value)
    {
        $money = static::findOne(['user_id' => $userId]);
        $money->amount = $value;
        $money->save();
        
        return $money;
    }
}
