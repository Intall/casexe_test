<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "things".
 *
 * @property int $id
 * @property int $user_id
 * @property string $data
 */
class Things extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'things';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'data'], 'required'],
            [['user_id'], 'integer'],
            [['data'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'data' => 'Data',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getThings($userId)
    {
        $arrayData = [];
        $things = static::findOne(['user_id' => $userId]);
        if (!empty($things['data'])) {
            $arrayData = unserialize($things['data']);
            $arNameAndAmount = [];
            foreach ($arrayData as $key => $value) {
                $arNameAndAmount[] = $value['name'] . ' - ' . $value['amount'] . 'pcs';
            }
            $strListThings = implode(', ', $arNameAndAmount);
        } else {
            $strListThings = 'not things';
        }

        return ['list' => $strListThings, 'data' => $arrayData];
    }

    /**
     * {@inheritdoc}
     */
    public static function addThings($userId, $data)
    {
        $things = static::findOne(['user_id' => $userId]);
        if (!empty($things)) {
            $things->data = serialize($data);
            $things->save();
        } else {
            $things = new Things();
            $things->user_id = $userId;
            $things->data = serialize($data);
            $things->save();
        }

        return $things;
    }
}
