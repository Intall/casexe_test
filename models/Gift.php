<?php

namespace app\models;

use app\models\Money;
use app\models\Things;
use app\models\Bonus;
use app\models\ThingsTypes;
use app\models\ExchangeMoneyToBonus;
use app\models\WarehouseThings;
use app\models\TransferMoney;

use Yii;

class Gift
{
    public $giftTypes;
    public $obMoney;
    public $obThings;
    public $obThingsTypes;
    public $obBonus;
    public $obExchangeMoneyToBonus;
    public $userId;
    public $anminUserId;
    public $intervalGiftMoney;
    public $intervalGiftBonus;
    public $coefficientToBonus;

    public function __construct()
    {
        $this->userId = Yii::$app->user->identity['id'];
        $this->userName = Yii::$app->user->identity['username'];
        $this->obMoney = new Money();
        $this->obThings = new Things();
        $this->obThingsTypes = new ThingsTypes();
        $this->obBonus = new Bonus();
        $this->obExchangeMoneyToBonus = new ExchangeMoneyToBonus();
        $this->obWarehouseThings = new WarehouseThings();
        $this->obTransferMoney = new TransferMoney();
        $this->anminUserId = Yii::$app->params['adminId'];
        $this->intervalGiftMoney = Yii::$app->params['intervalGiftMoney'];
        $this->intervalGiftBonus = Yii::$app->params['intervalGiftBonus'];
        $this->giftTypes = Yii::$app->params['giftTypes'];
        $this->coefficientToBonus = Yii::$app->params['coefficientToBonus'];
    }

    /**
     * {@inheritdoc}
     */
    public function getAllGift()
    {
        $arMoney = $this->obMoney->getMoney($this->userId);
        $arTransferMoney = $this->obTransferMoney->getTotalTransferMoney($this->userId);
        $arThings = $this->obThings->getThings($this->userId);
        $arBonus = $this->obBonus->getBonus($this->userId);

        $table = [
            'money' => $arMoney['amount'] . '$',
            'transferMoney' => (!empty($arTransferMoney)) ? $arTransferMoney . '$' : '-',
            'things' => $arThings['list'],
            'bonus' => $arBonus['amount'],
        ];

        return $table;
    }

    /**
     * {@inheritdoc}
     */
    public function getRandomGift()
    {
        $indexRandomGift = array_rand($this->filterOnlyAvailableGift($this->giftTypes));

        if (empty($indexRandomGift)) {
            return false;
        }

        switch ($indexRandomGift) {
            case 'money':
                // To know how much money is available and whether it is a number in the range
                $arMoneyAdmin = $this->obMoney->getMoney($this->anminUserId);
                $inInterval = $this->inIntervalMin($arMoneyAdmin['amount'], $this->intervalGiftMoney);
                if ($inInterval && !empty($arMoneyAdmin['amount'])) {
                    $key = hash('sha256', $this->userName . time() . rand(1000, 9999));
                    $randomAmount = rand($this->intervalGiftMoney[0], $this->intervalGiftMoney[1]);
                    if ($randomAmount > $arMoneyAdmin['amount']) {
                        $randomAmount = $arMoneyAdmin['amount'];
                    }
                    $exchangeBonus = $this->convertMoneyToBonus($randomAmount);
                    if ($exchangeBonus) {
                        $this->obExchangeMoneyToBonus->addExchangeMoneyToBonus($this->userId, $key, $randomAmount);
                        $gift = [
                            'name' => $indexRandomGift,
                            'amount' => $randomAmount,
                            'title' => $this->giftTypes[$indexRandomGift]['title'],
                            'message' => $this->giftTypes[$indexRandomGift]['message'] . ' ' . $this->coefficientToBonus . ' (You will receive ' . $exchangeBonus . ' bonuses)',
                            'textButtonExchangeBonus' => $this->giftTypes[$indexRandomGift]['exchangeBonus'],
                            'textButtonCancel' => $this->giftTypes[$indexRandomGift]['cancel'],
                            'key' => $key,
                        ];
                    } else {
                        $gift = [
                            'name' => $indexRandomGift,
                            'title' => 'Error exchange to bonus',
                        ];
                    }
                } else {
                    $this->giftTypes[$indexRandomGift]['available'] = false;
                    $gift = self::getRandomGift();
                }

                break;

            case 'things':
                $key = hash('sha256', $this->userName . time() . rand(1000, 9999));
                $thingsTypes = $this->obThingsTypes->getThingsTypes();
                $indexRandomThings = array_rand($this->filterOnlyAvailableThings($thingsTypes));
                if (!empty($indexRandomThings)) {
                    $randomThingsName = $thingsTypes[$indexRandomThings]['name'];
                    $randomThingsId = $thingsTypes[$indexRandomThings]['id'];
                    $this->obWarehouseThings->addThingsInWarehouse($this->userId, $key, $randomThingsName, $randomThingsId);

                    // Reserve
                    $this->obThingsTypes->reservThingsTypes($thingsTypes[$indexRandomThings]['id']);

                    $gift = [
                        'name' => $indexRandomGift,
                        'amount' => $randomThingsName,
                        'title' => $this->giftTypes[$indexRandomGift]['title'],
                        'message' => $this->giftTypes[$indexRandomGift]['message'],
                        'textButtonTake' => $this->giftTypes[$indexRandomGift]['take'],
                        'textButtonCancel' => $this->giftTypes[$indexRandomGift]['cancel'],
                        'key' => $key,
                    ];
                } else {
                    $this->giftTypes[$indexRandomGift]['available'] = false;
                    $gift = self::getRandomGift();
                }

                break;

            case 'bonus':
                $randomAmount = rand($this->intervalGiftBonus[0], $this->intervalGiftBonus[1]);
                $this->plusBonus($this->userId, $randomAmount);
                $gift = [
                    'name' => $indexRandomGift,
                    'amount' => $randomAmount,
                    'title' => $this->giftTypes[$indexRandomGift]['title'],
                ];

                break;

            default:

                break;
        }

        return $gift;
    }

    /**
     * {@inheritdoc}
     */
    public function plusBonus($userId = '', $amount = 0)
    {
        // Add bonus
        $arBonusUser = $this->obBonus->getBonus($this->userId);
        $newTotalBonusAmount = $arBonusUser['amount'] + $amount;

        return  $this->obBonus->updateBonus($this->userId, $newTotalBonusAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function filterOnlyAvailableGift($giftTypes)
    {
        foreach ($giftTypes as $key => $value) {
            if (empty($value['available'])) {
                unset($giftTypes[$key]);
            }
        }

        return $giftTypes;
    }

    /**
     * {@inheritdoc}
     */
    public function filterOnlyAvailableThings($things)
    {
        foreach ($things as $key => $value) {
            if (empty($value['amount'])) {
                unset($things[$key]);
            }
        }

        return $things;
    }

    /**
     * {@inheritdoc}
     */
    public function inIntervalMin($amount = 0, $interval = [])
    {
        if (!empty($amount) && !empty($interval)) {
            if ($amount >= $interval[0]) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function exchangeMoneyToBonus($key)
    {
        $arExchange = $this->obExchangeMoneyToBonus->getExchangeMoneyToBonus($key);

        if (!empty($arExchange['amount'])) {
            // Add bonus
            $bonusAmount = $this->convertMoneyToBonus($arExchange['amount']);
            $arBonusUser = $this->obBonus->getBonus($this->userId);
            $bonusAmount += $arBonusUser['amount'];
            $arBonusUser = $this->obBonus->updateBonus($this->userId, $bonusAmount);
        }

        return $this->obExchangeMoneyToBonus->removeExchangeMoneyToBonus($key);
    }

    /**
     * {@inheritdoc}
     */
    public function takeMoney($key)
    {
        $arExchange = $this->obExchangeMoneyToBonus->getExchangeMoneyToBonus($key);

        if (!empty($arExchange['amount'])) {
            // Add of money
            //
            $arMoneyAdminUser = $this->obMoney->getMoney($this->anminUserId);
            $this->obTransferMoney->addTransferMoney($this->userId, $arExchange['amount']);
            //$this->obMoney->updateMoney($this->userId, $arMoneyUser['amount'] + $arExchange['amount']);
            $this->obMoney->updateMoney($this->anminUserId, $arMoneyAdminUser['amount'] - $arExchange['amount']);

            //$this->requestAPIBank($arExchange['amount']);
        }

        return $this->obExchangeMoneyToBonus->removeExchangeMoneyToBonus($key);
    }

    /**
     * {@inheritdoc}
     */
    public function cancelThings($key)
    {
        $arReserve = $this->obWarehouseThings->getReserve($key);
        $this->obThingsTypes->unReservThingsTypes($arReserve['id_things']);
        $arRemove = $this->obWarehouseThings->removeThings($key);

        return $arRemove;
    }

    /**
     * {@inheritdoc}
     */
    public function takeThings($key)
    {
        $arReserve = $this->obWarehouseThings->takeThings($key);
        $arThings = $this->obThings->getThings($this->userId);
        if (!empty($arThings['data'])) {
            $data = $arThings['data'];
            if (isset($data[$arReserve['id_things']])) {
                $data[$arReserve['id_things']]['amount']++;
            } else {
                $data[$arReserve['id_things']] = [
                    'name' => $arReserve['name'],
                    'amount' => $arReserve['amount'],
                ];
            }
        } else {
            $data[$arReserve['id_things']] = [
                'name' => $arReserve['name'],
                'amount' => $arReserve['amount'],
            ];
        }

        $arTake = $this->obThings->addThings($this->userId, $data);

        return $arTake;
    }

    /**
     * {@inheritdoc}
     */
    public static function convertMoneyToBonus($money = 0)
    {
        $bonus = false;
        $coefficientToBonus = Yii::$app->params['coefficientToBonus'];
        if (!empty($money) && !empty($coefficientToBonus)) {
            $bonus = round($money * $coefficientToBonus);
        }

        return $bonus;
    }

}
