<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "warehouse_things".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int $amount
 * @property string $status
 * @property string $key_action
 */
class WarehouseThings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'warehouse_things';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'amount', 'status', 'key_action', 'id_things'], 'required'],
            [['user_id', 'amount', 'id_things'], 'integer'],
            [['name'], 'string'],
            [['status'], 'string', 'max' => 15],
            [['key_action'], 'string', 'max' => 70],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'amount' => 'Amount',
            'status' => 'Status',
            'key_action' => 'Key Action',
            'id_things' => 'ID Things',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function addThingsInWarehouse($userId, $key, $name, $idThings)
    {
        $reserve = new WarehouseThings();
        $reserve->user_id = $userId;
        $reserve->key_action = $key;
        $reserve->name = $name;
        $reserve->amount = 1;
        $reserve->status = 'reserve';
        $reserve->id_things = $idThings;
        $reserve->save();
        
        return $reserve;
    }

    /**
     * {@inheritdoc}
     */
    public static function getReserve($key)
    {
        $reserve = static::findOne(['key_action' => $key]);
        
        return $reserve;
    }

    /**
     * {@inheritdoc}
     */
    public static function removeThings($key)
    {
        $reserve = static::findOne(['key_action' => $key]);
        $reserve->delete();
        
        return $reserve;
    }

    /**
     * {@inheritdoc}
     */
    public static function takeThings($key)
    {
        $reserve = static::findOne(['key_action' => $key]);
        $reserve->status = 'to_send';
        $reserve->save();
        
        return $reserve;
    }

    /**
     * {@inheritdoc}
     */
    public static function getExchangeMoneyToBonus($key)
    {
        $arExchange = static::findOne(['key_exchange' => $key]);
        
        return $arExchange;
    }
}
