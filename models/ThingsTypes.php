<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "things_types".
 *
 * @property int $id
 * @property string $name
 * @property int $amount
 */
class ThingsTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'things_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'amount'], 'required'],
            [['name'], 'string'],
            [['amount'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getThingsTypes()
    {
        $arThingsTypes = static::find()->all();

        return $arThingsTypes;
    }

    /**
     * {@inheritdoc}
     */
    public static function reservThingsTypes($id)
    {
        $arThingsTypes = static::findOne(['id' => $id]);
        $arThingsTypes->amount = $arThingsTypes['amount'] - 1;
        $arThingsTypes->save();

        return $arThingsTypes;
    }

    /**
     * {@inheritdoc}
     */
    public static function unReservThingsTypes($id)
    {
        $arThingsTypes = static::findOne(['id' => $id]);
        $arThingsTypes->amount = $arThingsTypes['amount'] + 1;
        $arThingsTypes->save();

        return $arThingsTypes;
    }

}
