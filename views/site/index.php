<?php
/**
 * @var $this yii\web\View
 */
$this->title = 'CASEXE APPLICATION';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>
        <?php if ($isLogin): ?>
            <p><a class="btn btn-lg btn-success js-get-gift" href="#">Get a gift</a></p>
        <?php else: ?>
            <p class="bg-danger">Log on to the website to get the gift</p>
        <?php endif ?>
    </div>

    <div class="body-content js-body-content">
        <?php if ($isLogin): ?>
            <?=$this->render('/gift/table', ['gifts' => $gifts]); ?>
        <?php endif ?>
    </div>

</div>
