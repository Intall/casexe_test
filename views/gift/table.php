<?php
/**
 * @var $gifts array
 */
?>

<?php if (!empty($message)): ?>
    <div class="alert alert-info alert-dismissible fade in" role="alert">
        <h4><?=$message['title'] . ' ' . $message['amount'] ?></h4>

        <?php if (!empty($message['message'])): ?>
            <p><?=$message['message'] ?></p>
        <?php endif ?>

        <?php if (!empty($message['name']) && $message['name'] == 'money'): ?>
            <p>
                <button data-key='<?=$message['key'] ?>' type="button" class="js-exchange-money-to-bonus btn btn-info"><?=$message['textButtonExchangeBonus'] ?></button>
                <button type="button" data-key='<?=$message['key'] ?>' data-dismiss="alert" aria-label="Close" class="js-take-money btn btn-default"><?=$message['textButtonCancel'] ?></button>
            </p>
        <?php endif ?>

        <?php if (!empty($message['name']) && $message['name'] == 'things'): ?>
            <p>
                <button data-key='<?=$message['key'] ?>' type="button" class="js-take-things btn btn-info"><?=$message['textButtonTake'] ?></button>
                <button type="button" data-key='<?=$message['key'] ?>' data-dismiss="alert" aria-label="Close" class="js-cancel-things btn btn-default"><?=$message['textButtonCancel'] ?></button>
            </p>
        <?php endif ?>

    </div>
<?php endif ?>

<h2>Your gifts</h2>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>Money</th>
        <th>Transfer Money</th>
        <th>Things</th>
        <th>Bonus</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <?php foreach ($gifts as $key => $value): ?>
            <td><?=$value ?></td>
        <?php endforeach ?>
    </tr>
    </tbody>
</table>


